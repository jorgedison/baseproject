<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductoController extends Controller
{

    public function index(){
        $productos = Producto::all();

        return view('productos.index', compact('productos'));
    }

    public function create(){
        return view('productos.create');
    }

    public function store(Request $request){
      $request->validate([
        'nombre'=>'required',
        'precio'=> 'required|integer',
        'cantidad' => 'required|integer'
      ]);
      $producto = new Producto([
        'nombre' => $request->get('nombre'),
        'precio'=> $request->get('precio'),
        'cantidad'=> $request->get('cantidad')
      ]);
      $producto->save();
      return redirect('/productos')->with('success', 'Producto agregado');
    }

    public function show($id){
        //
    }

    public function edit($id){
        $producto = Producto::find($id);

        return view('productos.edit', compact('producto'));
    }

    public function update(Request $request, $id){
      $request->validate([
        'nombre'=>'required',
        'precio'=> 'required|integer',
        'cantidad' => 'required|integer'
      ]);

      $producto = Producto::find($id);
      $producto->nombre = $request->get('nombre');
      $producto->precio = $request->get('precio');
      $producto->cantidad = $request->get('cantidad');
      $producto->save();

      return redirect('/productos')->with('success', 'Producto actualizado');
    }

    public function destroy($id){
        $producto = Producto::find($id);
        $producto->delete();

      return redirect('/productos')->with('success', 'Producto eliminado');
    }
}
