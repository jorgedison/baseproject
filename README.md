# Proyecto Base Demo 1

###### Crear Proyecto Laravel 5.7

`composer create-project --prefer-dist laravel/laravel Demo1 5.7`

###### Generar Key del Proyecto
`php artisan key:generate`

###### Limpiar Cache
`php artisan config:cache`

###### Crear Modelo Producto
`php artisan make:model Producto -m`

###### Migrar Modelos a Tabla
`php artisan migrate`

###### Crear Controlador Producto
`php artisan make:controller ProductoController`

###### Listar rutas del Proyecto
`php artisan route:list`